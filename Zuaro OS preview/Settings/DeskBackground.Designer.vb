﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeskBackground
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DeskBackground))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.customwallpaper = New System.Windows.Forms.Panel()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.PictureBox16 = New System.Windows.Forms.Panel()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.StretchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ZoomToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.zoswallpaper = New System.Windows.Forms.Panel()
        Me.PictureBox29 = New System.Windows.Forms.PictureBox()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.PictureBox28 = New System.Windows.Forms.PictureBox()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.PictureBox24 = New System.Windows.Forms.PictureBox()
        Me.PictureBox25 = New System.Windows.Forms.PictureBox()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox54 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox53 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox52 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox51 = New System.Windows.Forms.PictureBox()
        Me.PictureBox50 = New System.Windows.Forms.PictureBox()
        Me.PictureBox49 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox48 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PictureBox47 = New System.Windows.Forms.PictureBox()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox46 = New System.Windows.Forms.PictureBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.PictureBox45 = New System.Windows.Forms.PictureBox()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.colorwallpaper = New System.Windows.Forms.Panel()
        Me.PictureBox64 = New System.Windows.Forms.PictureBox()
        Me.PictureBox43 = New System.Windows.Forms.PictureBox()
        Me.PictureBox63 = New System.Windows.Forms.PictureBox()
        Me.PictureBox55 = New System.Windows.Forms.PictureBox()
        Me.PictureBox62 = New System.Windows.Forms.PictureBox()
        Me.PictureBox56 = New System.Windows.Forms.PictureBox()
        Me.PictureBox61 = New System.Windows.Forms.PictureBox()
        Me.PictureBox57 = New System.Windows.Forms.PictureBox()
        Me.PictureBox60 = New System.Windows.Forms.PictureBox()
        Me.PictureBox58 = New System.Windows.Forms.PictureBox()
        Me.PictureBox59 = New System.Windows.Forms.PictureBox()
        Me.OpenWallpaperDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.customwallpaper.SuspendLayout()
        Me.PictureBox16.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.zoswallpaper.SuspendLayout()
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.colorwallpaper.SuspendLayout()
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.CloseNew
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Location = New System.Drawing.Point(492, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(40, 38)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'customwallpaper
        '
        Me.customwallpaper.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.customwallpaper.Controls.Add(Me.Button6)
        Me.customwallpaper.Controls.Add(Me.PictureBox16)
        Me.customwallpaper.Location = New System.Drawing.Point(12, 57)
        Me.customwallpaper.Name = "customwallpaper"
        Me.customwallpaper.Size = New System.Drawing.Size(520, 416)
        Me.customwallpaper.TabIndex = 15
        '
        'Button6
        '
        Me.Button6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button6.BackColor = System.Drawing.Color.Gainsboro
        Me.Button6.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue
        Me.Button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PowderBlue
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(5, 385)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(511, 27)
        Me.Button6.TabIndex = 3
        Me.Button6.Text = "Use as your Windows Background (Only currently shown background)"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'PictureBox16
        '
        Me.PictureBox16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox16.BackColor = System.Drawing.Color.Gainsboro
        Me.PictureBox16.BackgroundImage = CType(resources.GetObject("PictureBox16.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox16.Controls.Add(Me.PictureBox17)
        Me.PictureBox16.Location = New System.Drawing.Point(5, 5)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(511, 375)
        Me.PictureBox16.TabIndex = 2
        '
        'PictureBox17
        '
        Me.PictureBox17.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox17.ContextMenuStrip = Me.ContextMenuStrip1
        Me.PictureBox17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox17.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(511, 375)
        Me.PictureBox17.TabIndex = 0
        Me.PictureBox17.TabStop = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StretchToolStripMenuItem, Me.ZoomToolStripMenuItem, Me.TileToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(112, 70)
        '
        'StretchToolStripMenuItem
        '
        Me.StretchToolStripMenuItem.Name = "StretchToolStripMenuItem"
        Me.StretchToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.StretchToolStripMenuItem.Text = "Stretch"
        '
        'ZoomToolStripMenuItem
        '
        Me.ZoomToolStripMenuItem.Name = "ZoomToolStripMenuItem"
        Me.ZoomToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.ZoomToolStripMenuItem.Text = "Zoom"
        '
        'TileToolStripMenuItem
        '
        Me.TileToolStripMenuItem.Name = "TileToolStripMenuItem"
        Me.TileToolStripMenuItem.Size = New System.Drawing.Size(111, 22)
        Me.TileToolStripMenuItem.Text = "Tile"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Silver
        Me.Label15.Location = New System.Drawing.Point(305, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(70, 28)
        Me.Label15.TabIndex = 19
        Me.Label15.Text = "colour"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Silver
        Me.Label13.Location = New System.Drawing.Point(222, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(77, 28)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "picture"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Kozuka Gothic Pro R", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.YellowGreen
        Me.Label14.Location = New System.Drawing.Point(19, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(197, 28)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = "zuaro os wallpapers"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'zoswallpaper
        '
        Me.zoswallpaper.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.zoswallpaper.AutoScroll = True
        Me.zoswallpaper.Controls.Add(Me.PictureBox29)
        Me.zoswallpaper.Controls.Add(Me.PictureBox26)
        Me.zoswallpaper.Controls.Add(Me.PictureBox27)
        Me.zoswallpaper.Controls.Add(Me.PictureBox28)
        Me.zoswallpaper.Controls.Add(Me.PictureBox23)
        Me.zoswallpaper.Controls.Add(Me.PictureBox24)
        Me.zoswallpaper.Controls.Add(Me.PictureBox25)
        Me.zoswallpaper.Controls.Add(Me.PictureBox20)
        Me.zoswallpaper.Controls.Add(Me.PictureBox21)
        Me.zoswallpaper.Controls.Add(Me.PictureBox22)
        Me.zoswallpaper.Controls.Add(Me.PictureBox4)
        Me.zoswallpaper.Controls.Add(Me.PictureBox5)
        Me.zoswallpaper.Controls.Add(Me.PictureBox19)
        Me.zoswallpaper.Controls.Add(Me.PictureBox2)
        Me.zoswallpaper.Controls.Add(Me.PictureBox3)
        Me.zoswallpaper.Controls.Add(Me.PictureBox54)
        Me.zoswallpaper.Controls.Add(Me.PictureBox6)
        Me.zoswallpaper.Controls.Add(Me.PictureBox53)
        Me.zoswallpaper.Controls.Add(Me.PictureBox7)
        Me.zoswallpaper.Controls.Add(Me.PictureBox52)
        Me.zoswallpaper.Controls.Add(Me.PictureBox8)
        Me.zoswallpaper.Controls.Add(Me.PictureBox51)
        Me.zoswallpaper.Controls.Add(Me.PictureBox50)
        Me.zoswallpaper.Controls.Add(Me.PictureBox49)
        Me.zoswallpaper.Controls.Add(Me.PictureBox11)
        Me.zoswallpaper.Controls.Add(Me.PictureBox48)
        Me.zoswallpaper.Controls.Add(Me.PictureBox12)
        Me.zoswallpaper.Controls.Add(Me.PictureBox47)
        Me.zoswallpaper.Controls.Add(Me.PictureBox13)
        Me.zoswallpaper.Controls.Add(Me.PictureBox46)
        Me.zoswallpaper.Controls.Add(Me.PictureBox14)
        Me.zoswallpaper.Controls.Add(Me.PictureBox45)
        Me.zoswallpaper.Controls.Add(Me.PictureBox15)
        Me.zoswallpaper.Controls.Add(Me.PictureBox18)
        Me.zoswallpaper.Location = New System.Drawing.Point(12, 57)
        Me.zoswallpaper.Name = "zoswallpaper"
        Me.zoswallpaper.Size = New System.Drawing.Size(520, 416)
        Me.zoswallpaper.TabIndex = 14
        '
        'PictureBox29
        '
        Me.PictureBox29.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox29.BackgroundImage = CType(resources.GetObject("PictureBox29.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox29.Location = New System.Drawing.Point(14, 1238)
        Me.PictureBox29.Name = "PictureBox29"
        Me.PictureBox29.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox29.TabIndex = 36
        Me.PictureBox29.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox29, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox26
        '
        Me.PictureBox26.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox26.BackgroundImage = CType(resources.GetObject("PictureBox26.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox26.Location = New System.Drawing.Point(320, 1127)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox26.TabIndex = 35
        Me.PictureBox26.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox26, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox27
        '
        Me.PictureBox27.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox27.BackgroundImage = CType(resources.GetObject("PictureBox27.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox27.Location = New System.Drawing.Point(167, 1127)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox27.TabIndex = 34
        Me.PictureBox27.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox27, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox28
        '
        Me.PictureBox28.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox28.BackgroundImage = CType(resources.GetObject("PictureBox28.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox28.Location = New System.Drawing.Point(14, 1127)
        Me.PictureBox28.Name = "PictureBox28"
        Me.PictureBox28.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox28.TabIndex = 33
        Me.PictureBox28.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox28, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox23
        '
        Me.PictureBox23.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox23.BackgroundImage = CType(resources.GetObject("PictureBox23.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox23.Location = New System.Drawing.Point(320, 1016)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox23.TabIndex = 32
        Me.PictureBox23.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox23, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox24
        '
        Me.PictureBox24.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox24.BackgroundImage = CType(resources.GetObject("PictureBox24.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox24.Location = New System.Drawing.Point(167, 1016)
        Me.PictureBox24.Name = "PictureBox24"
        Me.PictureBox24.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox24.TabIndex = 31
        Me.PictureBox24.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox24, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox25
        '
        Me.PictureBox25.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox25.BackgroundImage = CType(resources.GetObject("PictureBox25.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox25.Location = New System.Drawing.Point(14, 1016)
        Me.PictureBox25.Name = "PictureBox25"
        Me.PictureBox25.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox25.TabIndex = 30
        Me.PictureBox25.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox25, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox20
        '
        Me.PictureBox20.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox20.BackgroundImage = CType(resources.GetObject("PictureBox20.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox20.Location = New System.Drawing.Point(320, 905)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox20.TabIndex = 29
        Me.PictureBox20.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox20, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox21
        '
        Me.PictureBox21.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox21.BackgroundImage = CType(resources.GetObject("PictureBox21.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox21.Location = New System.Drawing.Point(167, 905)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox21.TabIndex = 28
        Me.PictureBox21.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox21, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox22
        '
        Me.PictureBox22.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox22.BackgroundImage = CType(resources.GetObject("PictureBox22.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox22.Location = New System.Drawing.Point(14, 905)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox22.TabIndex = 27
        Me.PictureBox22.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox22, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox4.BackgroundImage = CType(resources.GetObject("PictureBox4.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox4.Location = New System.Drawing.Point(320, 794)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox4.TabIndex = 26
        Me.PictureBox4.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox4, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.BackgroundImage = CType(resources.GetObject("PictureBox5.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox5.Location = New System.Drawing.Point(167, 794)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox5.TabIndex = 25
        Me.PictureBox5.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox5, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox19
        '
        Me.PictureBox19.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox19.BackgroundImage = CType(resources.GetObject("PictureBox19.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox19.Location = New System.Drawing.Point(14, 794)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox19.TabIndex = 24
        Me.PictureBox19.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox19, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Location = New System.Drawing.Point(320, 683)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox2.TabIndex = 23
        Me.PictureBox2.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox2, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources._1440x900
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.Location = New System.Drawing.Point(167, 683)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox3.TabIndex = 22
        Me.PictureBox3.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox3, "Alone by memovaslg on DeviantArt. Thanks for permission to use (Borrow.)")
        '
        'PictureBox54
        '
        Me.PictureBox54.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox54.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.sleepy_kitten_by_amelka_wonka_kitty_d3ecixm
        Me.PictureBox54.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox54.Location = New System.Drawing.Point(14, 683)
        Me.PictureBox54.Name = "PictureBox54"
        Me.PictureBox54.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox54.TabIndex = 21
        Me.PictureBox54.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox54, "Sleepy Kitten by Amelka-Wonka-Kitty on DeviantArt. Thanks for permission to use (" & _
        "Borrow) this wallpaper")
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox6.Location = New System.Drawing.Point(14, 17)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(138, 105)
        Me.PictureBox6.TabIndex = 0
        Me.PictureBox6.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox6, "(Default)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "By the Zuaro Team")
        '
        'PictureBox53
        '
        Me.PictureBox53.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox53.BackgroundImage = CType(resources.GetObject("PictureBox53.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox53.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox53.Location = New System.Drawing.Point(320, 572)
        Me.PictureBox53.Name = "PictureBox53"
        Me.PictureBox53.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox53.TabIndex = 20
        Me.PictureBox53.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox53, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Summer_days
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox7.Location = New System.Drawing.Point(158, 17)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox7.TabIndex = 1
        Me.PictureBox7.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox7, "By the Hacker007. Thanks for the ability to use (borrow) this wallpaper.")
        '
        'PictureBox52
        '
        Me.PictureBox52.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox52.Location = New System.Drawing.Point(167, 572)
        Me.PictureBox52.Name = "PictureBox52"
        Me.PictureBox52.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox52.TabIndex = 19
        Me.PictureBox52.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox52, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox8
        '
        Me.PictureBox8.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox8.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources._____55_II_1920_1080
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox8.Location = New System.Drawing.Point(311, 17)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(181, 105)
        Me.PictureBox8.TabIndex = 2
        Me.PictureBox8.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox8, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox51
        '
        Me.PictureBox51.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox51.BackgroundImage = CType(resources.GetObject("PictureBox51.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox51.Location = New System.Drawing.Point(14, 572)
        Me.PictureBox51.Name = "PictureBox51"
        Me.PictureBox51.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox51.TabIndex = 18
        Me.PictureBox51.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox51, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox50
        '
        Me.PictureBox50.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox50.BackgroundImage = CType(resources.GetObject("PictureBox50.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox50.Location = New System.Drawing.Point(320, 461)
        Me.PictureBox50.Name = "PictureBox50"
        Me.PictureBox50.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox50.TabIndex = 17
        Me.PictureBox50.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox50, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox49
        '
        Me.PictureBox49.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox49.BackgroundImage = CType(resources.GetObject("PictureBox49.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox49.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox49.Location = New System.Drawing.Point(167, 461)
        Me.PictureBox49.Name = "PictureBox49"
        Me.PictureBox49.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox49.TabIndex = 16
        Me.PictureBox49.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox49, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox11
        '
        Me.PictureBox11.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox11.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Grid
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox11.Location = New System.Drawing.Point(14, 128)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox11.TabIndex = 5
        Me.PictureBox11.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox11, "Part of the Wallpapers_maybe pack by FediaFedia. Thanks for the ability to use (b" & _
        "orrow) this wallpaper.")
        '
        'PictureBox48
        '
        Me.PictureBox48.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox48.BackgroundImage = CType(resources.GetObject("PictureBox48.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox48.Location = New System.Drawing.Point(14, 461)
        Me.PictureBox48.Name = "PictureBox48"
        Me.PictureBox48.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox48.TabIndex = 15
        Me.PictureBox48.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox48, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox12
        '
        Me.PictureBox12.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox12.BackgroundImage = CType(resources.GetObject("PictureBox12.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox12.Location = New System.Drawing.Point(177, 128)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox12.TabIndex = 6
        Me.PictureBox12.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox12, "Part of the Wallpapers_maybe pack by FediaFedia. Thanks for the ability to use (b" & _
        "orrow) this wallpaper.")
        '
        'PictureBox47
        '
        Me.PictureBox47.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox47.BackgroundImage = CType(resources.GetObject("PictureBox47.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox47.Location = New System.Drawing.Point(320, 350)
        Me.PictureBox47.Name = "PictureBox47"
        Me.PictureBox47.Size = New System.Drawing.Size(172, 105)
        Me.PictureBox47.TabIndex = 14
        Me.PictureBox47.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox47, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox13
        '
        Me.PictureBox13.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox13.BackgroundImage = CType(resources.GetObject("PictureBox13.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox13.Location = New System.Drawing.Point(340, 128)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(152, 105)
        Me.PictureBox13.TabIndex = 7
        Me.PictureBox13.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox13, "Part of the Wallpapers_maybe pack by FediaFedia. Thanks for the ability to use (b" & _
        "orrow) this wallpaper.")
        '
        'PictureBox46
        '
        Me.PictureBox46.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox46.BackgroundImage = CType(resources.GetObject("PictureBox46.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox46.Location = New System.Drawing.Point(167, 350)
        Me.PictureBox46.Name = "PictureBox46"
        Me.PictureBox46.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox46.TabIndex = 13
        Me.PictureBox46.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox46, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox14
        '
        Me.PictureBox14.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox14.BackgroundImage = CType(resources.GetObject("PictureBox14.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox14.Location = New System.Drawing.Point(14, 239)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox14.TabIndex = 8
        Me.PictureBox14.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox14, "Part of the Wallpapers_maybe pack by FediaFedia. Thanks for the ability to use (b" & _
        "orrow) this wallpaper.")
        '
        'PictureBox45
        '
        Me.PictureBox45.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox45.BackgroundImage = CType(resources.GetObject("PictureBox45.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox45.Location = New System.Drawing.Point(14, 350)
        Me.PictureBox45.Name = "PictureBox45"
        Me.PictureBox45.Size = New System.Drawing.Size(147, 105)
        Me.PictureBox45.TabIndex = 12
        Me.PictureBox45.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox45, "By bo0xvn on DeviantArt Special thanks for permission to freely use.")
        '
        'PictureBox15
        '
        Me.PictureBox15.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox15.BackgroundImage = CType(resources.GetObject("PictureBox15.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox15.Location = New System.Drawing.Point(177, 239)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(157, 105)
        Me.PictureBox15.TabIndex = 9
        Me.PictureBox15.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox15, "Lake Forest by zaz369. Thanks for the ability to use (borrow) this wallpaper.")
        '
        'PictureBox18
        '
        Me.PictureBox18.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox18.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.windows_7_reimagined_wallpaper_by_gifteddeviant_d566kgg
        Me.PictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox18.Location = New System.Drawing.Point(340, 239)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(152, 105)
        Me.PictureBox18.TabIndex = 10
        Me.PictureBox18.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox18, "Windows 7 reimagined wallpaper by GiftedDeviant. Thanks for the ability to use (b" & _
        "orrow) this wallpaper.")
        '
        'colorwallpaper
        '
        Me.colorwallpaper.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.colorwallpaper.Controls.Add(Me.PictureBox64)
        Me.colorwallpaper.Controls.Add(Me.PictureBox43)
        Me.colorwallpaper.Controls.Add(Me.PictureBox63)
        Me.colorwallpaper.Controls.Add(Me.PictureBox55)
        Me.colorwallpaper.Controls.Add(Me.PictureBox62)
        Me.colorwallpaper.Controls.Add(Me.PictureBox56)
        Me.colorwallpaper.Controls.Add(Me.PictureBox61)
        Me.colorwallpaper.Controls.Add(Me.PictureBox57)
        Me.colorwallpaper.Controls.Add(Me.PictureBox60)
        Me.colorwallpaper.Controls.Add(Me.PictureBox58)
        Me.colorwallpaper.Controls.Add(Me.PictureBox59)
        Me.colorwallpaper.Location = New System.Drawing.Point(12, 57)
        Me.colorwallpaper.Name = "colorwallpaper"
        Me.colorwallpaper.Size = New System.Drawing.Size(520, 416)
        Me.colorwallpaper.TabIndex = 16
        '
        'PictureBox64
        '
        Me.PictureBox64.BackColor = System.Drawing.SystemColors.Highlight
        Me.PictureBox64.Location = New System.Drawing.Point(144, 76)
        Me.PictureBox64.Name = "PictureBox64"
        Me.PictureBox64.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox64.TabIndex = 10
        Me.PictureBox64.TabStop = False
        '
        'PictureBox43
        '
        Me.PictureBox43.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox43.Location = New System.Drawing.Point(14, 16)
        Me.PictureBox43.Name = "PictureBox43"
        Me.PictureBox43.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox43.TabIndex = 0
        Me.PictureBox43.TabStop = False
        '
        'PictureBox63
        '
        Me.PictureBox63.BackColor = System.Drawing.Color.HotPink
        Me.PictureBox63.Location = New System.Drawing.Point(79, 76)
        Me.PictureBox63.Name = "PictureBox63"
        Me.PictureBox63.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox63.TabIndex = 9
        Me.PictureBox63.TabStop = False
        '
        'PictureBox55
        '
        Me.PictureBox55.BackColor = System.Drawing.Color.Red
        Me.PictureBox55.Location = New System.Drawing.Point(79, 16)
        Me.PictureBox55.Name = "PictureBox55"
        Me.PictureBox55.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox55.TabIndex = 1
        Me.PictureBox55.TabStop = False
        '
        'PictureBox62
        '
        Me.PictureBox62.BackColor = System.Drawing.Color.Purple
        Me.PictureBox62.Location = New System.Drawing.Point(14, 76)
        Me.PictureBox62.Name = "PictureBox62"
        Me.PictureBox62.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox62.TabIndex = 8
        Me.PictureBox62.TabStop = False
        '
        'PictureBox56
        '
        Me.PictureBox56.BackColor = System.Drawing.Color.Orange
        Me.PictureBox56.Location = New System.Drawing.Point(144, 16)
        Me.PictureBox56.Name = "PictureBox56"
        Me.PictureBox56.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox56.TabIndex = 2
        Me.PictureBox56.TabStop = False
        '
        'PictureBox61
        '
        Me.PictureBox61.BackColor = System.Drawing.Color.DodgerBlue
        Me.PictureBox61.Location = New System.Drawing.Point(469, 16)
        Me.PictureBox61.Name = "PictureBox61"
        Me.PictureBox61.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox61.TabIndex = 7
        Me.PictureBox61.TabStop = False
        '
        'PictureBox57
        '
        Me.PictureBox57.BackColor = System.Drawing.Color.Yellow
        Me.PictureBox57.Location = New System.Drawing.Point(209, 16)
        Me.PictureBox57.Name = "PictureBox57"
        Me.PictureBox57.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox57.TabIndex = 3
        Me.PictureBox57.TabStop = False
        '
        'PictureBox60
        '
        Me.PictureBox60.BackColor = System.Drawing.Color.SteelBlue
        Me.PictureBox60.Location = New System.Drawing.Point(404, 16)
        Me.PictureBox60.Name = "PictureBox60"
        Me.PictureBox60.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox60.TabIndex = 6
        Me.PictureBox60.TabStop = False
        '
        'PictureBox58
        '
        Me.PictureBox58.BackColor = System.Drawing.Color.YellowGreen
        Me.PictureBox58.Location = New System.Drawing.Point(274, 16)
        Me.PictureBox58.Name = "PictureBox58"
        Me.PictureBox58.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox58.TabIndex = 4
        Me.PictureBox58.TabStop = False
        '
        'PictureBox59
        '
        Me.PictureBox59.BackColor = System.Drawing.Color.OliveDrab
        Me.PictureBox59.Location = New System.Drawing.Point(339, 16)
        Me.PictureBox59.Name = "PictureBox59"
        Me.PictureBox59.Size = New System.Drawing.Size(59, 54)
        Me.PictureBox59.TabIndex = 5
        Me.PictureBox59.TabStop = False
        '
        'OpenWallpaperDialog1
        '
        Me.OpenWallpaperDialog1.FileName = "wallpaper"
        Me.OpenWallpaperDialog1.Filter = "Images|*.jpg;*.png;*.bmp;*.gif;*.jpg"
        Me.OpenWallpaperDialog1.InitialDirectory = "C:\Users\"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 5000
        Me.ToolTip1.BackColor = System.Drawing.Color.White
        Me.ToolTip1.ForeColor = System.Drawing.Color.Black
        Me.ToolTip1.InitialDelay = 5
        Me.ToolTip1.ReshowDelay = 10
        '
        'DeskBackground
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(544, 485)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.zoswallpaper)
        Me.Controls.Add(Me.colorwallpaper)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.customwallpaper)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DeskBackground"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DeskBackground"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.customwallpaper.ResumeLayout(False)
        Me.PictureBox16.ResumeLayout(False)
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.zoswallpaper.ResumeLayout(False)
        CType(Me.PictureBox29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.colorwallpaper.ResumeLayout(False)
        CType(Me.PictureBox64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox59, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents customwallpaper As System.Windows.Forms.Panel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents PictureBox16 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents zoswallpaper As System.Windows.Forms.Panel
    Friend WithEvents PictureBox54 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox53 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox52 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox51 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox50 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox49 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox48 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox47 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox46 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox45 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents colorwallpaper As System.Windows.Forms.Panel
    Friend WithEvents PictureBox64 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox43 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox63 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox55 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox62 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox56 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox61 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox57 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox60 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox58 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox59 As System.Windows.Forms.PictureBox
    Friend WithEvents OpenWallpaperDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents StretchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ZoomToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox29 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox26 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox27 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox28 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox23 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox24 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox25 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox21 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox22 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
End Class
