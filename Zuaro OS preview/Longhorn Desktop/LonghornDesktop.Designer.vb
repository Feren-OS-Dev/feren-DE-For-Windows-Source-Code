﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LonghornDesktop
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LonghornDesktop))
        Dim ClockStyleData1 As DevComponents.DotNetBar.Controls.ClockStyleData = New DevComponents.DotNetBar.Controls.ClockStyleData()
        Dim ColorData1 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Dim ColorData2 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Dim ColorData3 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Dim ClockHandStyleData1 As DevComponents.DotNetBar.Controls.ClockHandStyleData = New DevComponents.DotNetBar.Controls.ClockHandStyleData()
        Dim ColorData4 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Dim ColorData5 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Dim ClockHandStyleData2 As DevComponents.DotNetBar.Controls.ClockHandStyleData = New DevComponents.DotNetBar.Controls.ClockHandStyleData()
        Dim ColorData6 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Dim ClockHandStyleData3 As DevComponents.DotNetBar.Controls.ClockHandStyleData = New DevComponents.DotNetBar.Controls.ClockHandStyleData()
        Dim ColorData7 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Dim ColorData8 As DevComponents.DotNetBar.Controls.ColorData = New DevComponents.DotNetBar.Controls.ColorData()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.StepIndicator1 = New DevComponents.DotNetBar.Controls.StepIndicator()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.PictureBox26 = New System.Windows.Forms.PictureBox()
        Me.PictureBox27 = New System.Windows.Forms.PictureBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TimeandDate = New System.Windows.Forms.Label()
        Me.AnalogClockControl1 = New DevComponents.DotNetBar.Controls.AnalogClockControl()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Reload = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Taskbar
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 528)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(657, 31)
        Me.Panel1.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Taskbar_logo_new
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(64, 31)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'StepIndicator1
        '
        Me.StepIndicator1.CurrentStep = 0
        Me.StepIndicator1.Location = New System.Drawing.Point(-112, 0)
        Me.StepIndicator1.Name = "StepIndicator1"
        Me.StepIndicator1.Size = New System.Drawing.Size(160, 10)
        Me.StepIndicator1.TabIndex = 1
        Me.StepIndicator1.Text = "StepIndicator1"
        Me.StepIndicator1.Visible = False
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Taskbar
        Me.Panel2.Controls.Add(Me.PictureBox23)
        Me.Panel2.Controls.Add(Me.PictureBox22)
        Me.Panel2.Controls.Add(Me.PictureBox21)
        Me.Panel2.Controls.Add(Me.PictureBox20)
        Me.Panel2.Controls.Add(Me.PictureBox19)
        Me.Panel2.Controls.Add(Me.PictureBox18)
        Me.Panel2.Controls.Add(Me.PictureBox17)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Location = New System.Drawing.Point(8, 159)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(363, 362)
        Me.Panel2.TabIndex = 2
        Me.Panel2.Visible = False
        '
        'PictureBox23
        '
        Me.PictureBox23.BackgroundImage = CType(resources.GetObject("PictureBox23.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox23.Location = New System.Drawing.Point(44, 327)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(31, 28)
        Me.PictureBox23.TabIndex = 9
        Me.PictureBox23.TabStop = False
        '
        'PictureBox22
        '
        Me.PictureBox22.BackgroundImage = CType(resources.GetObject("PictureBox22.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox22.Location = New System.Drawing.Point(9, 327)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(31, 28)
        Me.PictureBox22.TabIndex = 8
        Me.PictureBox22.TabStop = False
        '
        'PictureBox21
        '
        Me.PictureBox21.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Computer
        Me.PictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox21.Location = New System.Drawing.Point(303, 276)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(48, 45)
        Me.PictureBox21.TabIndex = 7
        Me.PictureBox21.TabStop = False
        '
        'PictureBox20
        '
        Me.PictureBox20.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Film_Folder
        Me.PictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox20.Location = New System.Drawing.Point(303, 225)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(48, 45)
        Me.PictureBox20.TabIndex = 6
        Me.PictureBox20.TabStop = False
        '
        'PictureBox19
        '
        Me.PictureBox19.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Musique_folder
        Me.PictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox19.Location = New System.Drawing.Point(303, 174)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(48, 45)
        Me.PictureBox19.TabIndex = 5
        Me.PictureBox19.TabStop = False
        '
        'PictureBox18
        '
        Me.PictureBox18.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Photo_Folder
        Me.PictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox18.Location = New System.Drawing.Point(303, 123)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(48, 45)
        Me.PictureBox18.TabIndex = 4
        Me.PictureBox18.TabStop = False
        '
        'PictureBox17
        '
        Me.PictureBox17.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.File_folder
        Me.PictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox17.Location = New System.Drawing.Point(303, 70)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(48, 45)
        Me.PictureBox17.TabIndex = 3
        Me.PictureBox17.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.AutoScroll = True
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Controls.Add(Me.PictureBox16)
        Me.Panel3.Controls.Add(Me.Label14)
        Me.Panel3.Controls.Add(Me.PictureBox15)
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.PictureBox14)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Controls.Add(Me.PictureBox13)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.PictureBox12)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.PictureBox11)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.PictureBox10)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.PictureBox9)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.PictureBox8)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.PictureBox7)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.PictureBox6)
        Me.Panel3.Controls.Add(Me.PictureBox5)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.PictureBox4)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.PictureBox3)
        Me.Panel3.Location = New System.Drawing.Point(7, 70)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(290, 251)
        Me.Panel3.TabIndex = 2
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(55, 630)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(103, 17)
        Me.Label15.TabIndex = 25
        Me.Label15.Text = "Zuaro Weather"
        '
        'PictureBox16
        '
        Me.PictureBox16.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Nuageux
        Me.PictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox16.Location = New System.Drawing.Point(6, 617)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox16.TabIndex = 24
        Me.PictureBox16.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(55, 576)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(93, 34)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Zuaro Anti-" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ransomware"
        '
        'PictureBox15
        '
        Me.PictureBox15.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Défender
        Me.PictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox15.Location = New System.Drawing.Point(6, 570)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox15.TabIndex = 22
        Me.PictureBox15.TabStop = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(55, 536)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(95, 17)
        Me.Label13.TabIndex = 21
        Me.Label13.Text = "Multi Desktop"
        '
        'PictureBox14
        '
        Me.PictureBox14.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.MultiDesk
        Me.PictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox14.Location = New System.Drawing.Point(6, 523)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox14.TabIndex = 20
        Me.PictureBox14.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(55, 489)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(61, 17)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "Terminal"
        '
        'PictureBox13
        '
        Me.PictureBox13.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Terminal
        Me.PictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox13.Location = New System.Drawing.Point(6, 476)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox13.TabIndex = 18
        Me.PictureBox13.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(55, 442)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 17)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Look Changer"
        '
        'PictureBox12
        '
        Me.PictureBox12.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Apparence
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox12.Location = New System.Drawing.Point(6, 429)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox12.TabIndex = 16
        Me.PictureBox12.TabStop = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(55, 395)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 17)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Settings"
        '
        'PictureBox11
        '
        Me.PictureBox11.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Parametre
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox11.Location = New System.Drawing.Point(6, 382)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox11.TabIndex = 14
        Me.PictureBox11.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(55, 348)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 17)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Notepad"
        '
        'PictureBox10
        '
        Me.PictureBox10.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.NotePad
        Me.PictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox10.Location = New System.Drawing.Point(6, 335)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox10.TabIndex = 12
        Me.PictureBox10.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(55, 301)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 17)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Gallery"
        '
        'PictureBox9
        '
        Me.PictureBox9.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Photo
        Me.PictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox9.Location = New System.Drawing.Point(6, 288)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox9.TabIndex = 10
        Me.PictureBox9.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(55, 254)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(77, 17)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Calculator"
        '
        'PictureBox8
        '
        Me.PictureBox8.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Calculator
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox8.Location = New System.Drawing.Point(6, 241)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox8.TabIndex = 8
        Me.PictureBox8.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(55, 207)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 17)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Calendar"
        '
        'PictureBox7
        '
        Me.PictureBox7.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Calendrier_avec_Date
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox7.Location = New System.Drawing.Point(6, 194)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox7.TabIndex = 6
        Me.PictureBox7.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(55, 160)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 17)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Zuaro Store"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(55, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 17)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Ri Player"
        '
        'PictureBox6
        '
        Me.PictureBox6.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Store
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox6.Location = New System.Drawing.Point(6, 147)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox6.TabIndex = 4
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Movies
        Me.PictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox5.Location = New System.Drawing.Point(6, 100)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox5.TabIndex = 4
        Me.PictureBox5.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(55, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Zuaro Explorer"
        '
        'PictureBox4
        '
        Me.PictureBox4.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Explorer
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox4.Location = New System.Drawing.Point(6, 53)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox4.TabIndex = 2
        Me.PictureBox4.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(55, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 34)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "KittenFox" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Web Browser"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Kittenfox_Internet
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox3.Location = New System.Drawing.Point(6, 6)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(42, 41)
        Me.PictureBox3.TabIndex = 0
        Me.PictureBox3.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(74, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 21)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Dominic Hayes"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox2.Location = New System.Drawing.Point(7, 8)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(59, 55)
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.ZOS_Desk_White
        Me.Panel4.Controls.Add(Me.Panel6)
        Me.Panel4.Controls.Add(Me.Panel5)
        Me.Panel4.Controls.Add(Me.TimeandDate)
        Me.Panel4.Controls.Add(Me.AnalogClockControl1)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel4.Location = New System.Drawing.Point(657, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(165, 559)
        Me.Panel4.TabIndex = 3
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.PictureBox26)
        Me.Panel6.Controls.Add(Me.PictureBox27)
        Me.Panel6.Location = New System.Drawing.Point(5, 40)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(157, 27)
        Me.Panel6.TabIndex = 3
        '
        'PictureBox26
        '
        Me.PictureBox26.ErrorImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Explorer
        Me.PictureBox26.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Explorer
        Me.PictureBox26.InitialImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Explorer
        Me.PictureBox26.Location = New System.Drawing.Point(40, 1)
        Me.PictureBox26.Name = "PictureBox26"
        Me.PictureBox26.Size = New System.Drawing.Size(27, 27)
        Me.PictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox26.TabIndex = 1
        Me.PictureBox26.TabStop = False
        '
        'PictureBox27
        '
        Me.PictureBox27.ErrorImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Kittenfox_Internet
        Me.PictureBox27.Image = Global.Feren_OS_DE_preview.My.Resources.Resources.Kittenfox_Internet
        Me.PictureBox27.InitialImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Kittenfox_Internet
        Me.PictureBox27.Location = New System.Drawing.Point(7, 1)
        Me.PictureBox27.Name = "PictureBox27"
        Me.PictureBox27.Size = New System.Drawing.Size(27, 27)
        Me.PictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox27.TabIndex = 0
        Me.PictureBox27.TabStop = False
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.TextBox1)
        Me.Panel5.Location = New System.Drawing.Point(5, 6)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(157, 27)
        Me.Panel5.TabIndex = 2
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.Black
        Me.TextBox1.Location = New System.Drawing.Point(4, 3)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(150, 22)
        Me.TextBox1.TabIndex = 0
        '
        'TimeandDate
        '
        Me.TimeandDate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.TimeandDate.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TimeandDate.ForeColor = System.Drawing.Color.Black
        Me.TimeandDate.Location = New System.Drawing.Point(2, 526)
        Me.TimeandDate.Name = "TimeandDate"
        Me.TimeandDate.Size = New System.Drawing.Size(162, 23)
        Me.TimeandDate.TabIndex = 1
        Me.TimeandDate.Text = "00:00:00 00/00/00"
        Me.TimeandDate.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'AnalogClockControl1
        '
        Me.AnalogClockControl1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.AnalogClockControl1.ClockStyle = DevComponents.DotNetBar.Controls.eClockStyles.Custom
        ColorData1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        ColorData1.BorderWidth = 0.01!
        ColorData1.BrushSBSScale = 1.0!
        ColorData1.BrushType = DevComponents.DotNetBar.Controls.eBrushTypes.Linear
        ColorData1.Color1 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        ColorData1.Color2 = System.Drawing.Color.FromArgb(CType(CType(152, Byte), Integer), CType(CType(152, Byte), Integer), CType(CType(152, Byte), Integer))
        ClockStyleData1.BezelColor = ColorData1
        ColorData2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData2.BorderWidth = 0.01!
        ColorData2.BrushSBSScale = 1.0!
        ColorData2.Color1 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData2.Color2 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ClockStyleData1.CapColor = ColorData2
        ColorData3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(145, Byte), Integer), CType(CType(161, Byte), Integer))
        ColorData3.BorderWidth = 0.01!
        ColorData3.BrushAngle = 45.0!
        ColorData3.BrushSBSScale = 1.0!
        ColorData3.BrushType = DevComponents.DotNetBar.Controls.eBrushTypes.Linear
        ColorData3.Color1 = System.Drawing.Color.FromArgb(CType(CType(191, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(213, Byte), Integer))
        ColorData3.Color2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        ClockStyleData1.FaceColor = ColorData3
        ClockStyleData1.GlassAngle = -20
        ColorData4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData4.BorderWidth = 0.01!
        ColorData4.BrushSBSScale = 1.0!
        ColorData4.Color1 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData4.Color2 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ClockHandStyleData1.HandColor = ColorData4
        ClockHandStyleData1.Length = 0.55!
        ClockHandStyleData1.Width = 0.015!
        ClockStyleData1.HourHandStyle = ClockHandStyleData1
        ColorData5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        ColorData5.BorderWidth = 0.01!
        ColorData5.BrushSBSScale = 1.0!
        ColorData5.BrushType = DevComponents.DotNetBar.Controls.eBrushTypes.Linear
        ColorData5.Color1 = System.Drawing.Color.FromArgb(CType(CType(122, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(154, Byte), Integer))
        ColorData5.Color2 = System.Drawing.Color.FromArgb(CType(CType(122, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(154, Byte), Integer))
        ClockStyleData1.LargeTickColor = ColorData5
        ColorData6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData6.BorderWidth = 0.01!
        ColorData6.BrushSBSScale = 1.0!
        ColorData6.Color1 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData6.Color2 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ClockHandStyleData2.HandColor = ColorData6
        ClockHandStyleData2.Length = 0.8!
        ClockHandStyleData2.Width = 0.01!
        ClockStyleData1.MinuteHandStyle = ClockHandStyleData2
        ClockStyleData1.NumberFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        ColorData7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData7.BorderWidth = 0.01!
        ColorData7.BrushSBSScale = 1.0!
        ColorData7.Color1 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ColorData7.Color2 = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(138, Byte), Integer))
        ClockHandStyleData3.HandColor = ColorData7
        ClockHandStyleData3.HandStyle = DevComponents.DotNetBar.Controls.eHandStyles.Style2
        ClockHandStyleData3.Length = 0.8!
        ClockHandStyleData3.Width = 0.005!
        ClockStyleData1.SecondHandStyle = ClockHandStyleData3
        ColorData8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        ColorData8.BorderWidth = 0.01!
        ColorData8.BrushSBSScale = 1.0!
        ColorData8.BrushType = DevComponents.DotNetBar.Controls.eBrushTypes.Linear
        ColorData8.Color1 = System.Drawing.Color.FromArgb(CType(CType(122, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(154, Byte), Integer))
        ColorData8.Color2 = System.Drawing.Color.FromArgb(CType(CType(122, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(154, Byte), Integer))
        ClockStyleData1.SmallTickColor = ColorData8
        ClockStyleData1.Style = DevComponents.DotNetBar.Controls.eClockStyles.Custom
        Me.AnalogClockControl1.ClockStyleData = ClockStyleData1
        Me.AnalogClockControl1.Location = New System.Drawing.Point(32, 424)
        Me.AnalogClockControl1.Name = "AnalogClockControl1"
        Me.AnalogClockControl1.Size = New System.Drawing.Size(100, 100)
        Me.AnalogClockControl1.TabIndex = 0
        Me.AnalogClockControl1.Text = "AnalogClockControl1"
        Me.AnalogClockControl1.TimeZone = "UTC"
        Me.AnalogClockControl1.Value = New Date(2013, 12, 20, 8, 4, 39, 625)
        '
        'Timer1
        '
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(509, 471)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(142, 51)
        Me.Label16.TabIndex = 4
        Me.Label16.Text = "Zuaro OS Beta 1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Build v.1.0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "zuaroos.weebly.com"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'Reload
        '
        Me.Reload.Interval = 750
        '
        'LonghornDesktop
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BackgroundImage = Global.Feren_OS_DE_preview.My.Resources.Resources.Home_1_Month_Activate_
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(822, 559)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.StepIndicator1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "LonghornDesktop"
        Me.Text = "Desktop - Zuaro OS"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        CType(Me.PictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents StepIndicator1 As DevComponents.DotNetBar.Controls.StepIndicator
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox21 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox22 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox23 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents AnalogClockControl1 As DevComponents.DotNetBar.Controls.AnalogClockControl
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TimeandDate As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox26 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox27 As System.Windows.Forms.PictureBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Reload As System.Windows.Forms.Timer

End Class
